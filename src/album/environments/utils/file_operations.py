"""Utils for file operations."""
import errno
import os
import shutil
import stat
import sys
from pathlib import Path
from typing import Any, Callable, Dict, Tuple, Union

import yaml
from album.runner import album_logging

module_logger = album_logging.get_active_logger
enc = sys.getfilesystemencoding()

win_shell = None


def get_dict_from_yml(yml_file: Union[str, Path]) -> Dict[str, Any]:
    """Read a dictionary from a file in yml format."""
    with open(yml_file) as yml_f:
        d = yaml.safe_load(yml_f)

    if not isinstance(d, dict):
        raise TypeError("Yaml file %s invalid!" % str(yml_file))

    return d


def write_dict_to_yml(yml_file: Union[str, Path], d: Dict[str, Any]) -> bool:
    """Write a dictionary to a file in yml format."""
    yml_file = Path(yml_file)
    create_path_recursively(yml_file.parent)

    with open(yml_file, "w+") as yml_f:
        yml_f.write(yaml.dump(d, Dumper=yaml.Dumper))

    return True


def create_path_recursively(path: Union[str, Path]) -> bool:
    """Create a path; Create missing parent folders."""
    p = Path(path)
    p.mkdir(parents=True, exist_ok=True)

    return True


def copy_folder(
    folder_to_copy: Union[str, Path],
    destination: Union[str, Path],
    copy_root_folder: bool = True,
    force_copy: bool = False,
) -> Path:
    """Copy a folder to a destination.

    Args:
        folder_to_copy:
            The folder to copy
        destination:
            The destination folder to copy to
        copy_root_folder:
            boolean value. if true copies the root folder in the target destination.
            Else all files in the folder to copy.
        force_copy:
            boolean value. If true, removes the destination folder before copying.

    """
    folder_to_copy = Path(folder_to_copy)
    destination = Path(destination)

    if os.path.exists(destination) and os.path.samefile(folder_to_copy, destination):
        return destination

    if copy_root_folder:
        destination = destination.joinpath(folder_to_copy.name)

    if force_copy:
        force_remove(destination)

    create_path_recursively(destination)

    for root, dirs, files in os.walk(folder_to_copy):
        _root = Path(root)

        for d in dirs:
            _d = Path(d)
            copy_folder(
                _root.joinpath(_d), destination.joinpath(_d), copy_root_folder=False
            )
        for fi in files:
            copy(_root.joinpath(fi), destination)
        break

    return destination


def copy(file: Union[str, Path], path_to: Union[str, Path]) -> Path:
    """Copy a file A to either folder B or file B; Make sure folder structure for target exists."""
    file = Path(file)
    path_to = Path(path_to)

    if os.path.exists(path_to) and os.path.samefile(file, path_to):
        return path_to

    create_path_recursively(path_to.parent)

    return Path(shutil.copy(file, path_to))


def force_remove(path: Union[str, Path], warning: bool = True) -> None:
    """Remove a file or folder; If it is read-only, change the flag."""
    path = Path(path)
    if path.exists():
        try:
            if path.is_file():
                try:
                    path.unlink()
                except PermissionError:
                    handle_remove_readonly(os.unlink, path, sys.exc_info())
            else:
                shutil.rmtree(
                    str(path), ignore_errors=False, onerror=handle_remove_readonly
                )
        except PermissionError as e:
            module_logger().warn("Cannot delete %s." % str(path))
            if not warning:
                raise e


def handle_remove_readonly(
    func: Callable,
    path: Union[str, Path],
    exc: Tuple[Any, Any, Any],
) -> None:
    """Change readonly flag of a given path."""
    excvalue = exc[1]
    if func in (os.rmdir, os.remove, os.unlink) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)  # 0777
        func(path)
    else:
        raise
