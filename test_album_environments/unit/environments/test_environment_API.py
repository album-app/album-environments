import unittest.mock

from album.environments.environment_api import EnvironmentAPI
from test_album_environments.test_common import TestCommon


class TestEnvironmentAPI(TestCommon):
    def setUp(self):
        super().setUp()
        # FIXME
        self.environment_manager = EnvironmentAPI()

    def tearDown(self) -> None:
        super().tearDown()

    @unittest.skip("Needs to be implemented!")
    def test_install_environment(self):
        # ToDo: implement!
        pass

    @unittest.skip("Needs to be implemented!")
    def test_run_script(self):
        # ToDo: implement!
        pass

    @unittest.skip("Needs to be implemented!")
    def test_get_package_manager(self):
        # ToDo: implement!
        pass

    @unittest.skip("Needs to be implemented!")
    def test__prepare_env_file(self):
        # ToDo: implement!
        pass

    @unittest.skip("Needs to be implemented!")
    def test_get_installed_package_manager(self):
        # ToDo: implement!
        pass
